package basic.android.registerform.utils;

import android.app.Application;
import android.graphics.Typeface;

import com.orm.SugarApp;
import com.orm.SugarContext;

public class BaseApplication extends SugarApp {
    public static BaseApplication baseApp;
    public static Typeface typeface;

    @Override
    public void onCreate() {
        super.onCreate();  // in sugarApp -> create , close DB
        baseApp = this;
        typeface= Typeface.createFromAsset(getAssets() , Constants.appFontName);

        SugarContext.init(this);

    }
}
