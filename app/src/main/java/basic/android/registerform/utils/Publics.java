package basic.android.registerform.utils;

import android.app.Activity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

public class Publics {

    public static void toast(String msg) {
        Toast.makeText(BaseApplication.baseApp, msg, Toast.LENGTH_SHORT).show();
    }


    public static void closeKeyBoard(View v) {
        try {
            InputMethodManager imm = (InputMethodManager) BaseApplication.baseApp.getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        } catch (Exception e) {
            //cannot close
        }

    }


}
