package basic.android.registerform.customViews;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import basic.android.registerform.utils.BaseApplication;

public class MyEditText extends AppCompatEditText {
    public MyEditText(Context context) {
        super(context);
        this.setTypeface(BaseApplication.typeface);
    }


    public MyEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(BaseApplication.typeface);
    }

    public MyEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.setTypeface(BaseApplication.typeface);
    }

    public String text() {
        return (this.getText() != null ? this.getText().toString() : "");
    }
}
