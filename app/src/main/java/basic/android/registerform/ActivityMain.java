package basic.android.registerform;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.view.View;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import basic.android.registerform.customViews.MyButton;
import basic.android.registerform.customViews.MyEditText;
import basic.android.registerform.customViews.MyImageView;
import basic.android.registerform.recycler.RecyclerActivity;
import basic.android.registerform.recycler.RegistrationInfoModel;
import basic.android.registerform.utils.BaseActivity;
import basic.android.registerform.utils.Publics;

public class ActivityMain extends BaseActivity {

    RoundedImageView imgAvatar;
    Bitmap bitmap;
    MyEditText txtName, txtLastname, txtEmail, txtUsername, txtPassword;
    List<RegistrationInfoModel> registrationInfoModelList;
    RegistrationInfoModel registrationInfoModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bind();

    }

    void bind() {
        registrationInfoModel = new RegistrationInfoModel();
        imgAvatar = ((RoundedImageView) findViewById(R.id.imgAvatar));


        ((MyButton) findViewById(R.id.btnAdd)).setOnClickListener(v -> {

            txtName = (MyEditText) findViewById(R.id.txtName);
            txtLastname = (MyEditText) findViewById(R.id.txtLastname);
            txtEmail = (MyEditText) findViewById(R.id.txtEmail);

            registrationInfoModel.setName(txtName.text());
            registrationInfoModel.setLastname(txtLastname.text());
            registrationInfoModel.setEmail(txtEmail.text());

            addUserToDB(registrationInfoModel);

            gotoRecyclerActivity();


        });


        ((MyImageView) findViewById(R.id.imgCamera)).setOnClickListener(v -> {

            openCustomDialog();


        });


    }


    boolean checkCameraPermission() {
        if (ContextCompat.checkSelfPermission(mContext,
                Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            return openCamera();
        } else {
            ActivityCompat.requestPermissions(mActivity,
                    new String[]{Manifest.permission.CAMERA},
                    1000);
            return false;
        }

    }

    boolean checkGalleryPermission() {


        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return openGallery();
        } else {

            ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2000);
            return false;
        }

    }


    boolean openCamera() {


        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);



        startActivityForResult(intent, 1000);


        return true;
    }



    boolean openGallery() {


        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select picture"), 2000 );


        return true;


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1000) {
            if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                //from camera check permission
                Publics.toast("مجوز دسترسی به دوربین داده نشد!");

        } else if (requestCode == 2000) {
            if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)

                Publics.toast("مجوز دسترسی به عکس ها داده نشد!");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == 1000) {
            if (resultCode == Activity.RESULT_OK) {

                Bitmap bitmap = (Bitmap) intent.getExtras().get("data");

    Glide.with(mContext)
            .load(bitmap)
            .into(imgAvatar);

                // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                Uri tempUri = getImageUri(getApplicationContext(), bitmap);

                registrationInfoModel.setAvatarpath(getRealPathFromURI(tempUri));
}

        } else if (requestCode == 2000) {

            if (resultCode == RESULT_OK) {
                try {

                    final Uri imageUri = intent.getData();

                    final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                    bitmap = BitmapFactory.decodeStream(imageStream);
                    Glide.with(mContext)
                            .load(bitmap)
                            .into(imgAvatar);

                    registrationInfoModel.setAvatarpath(getRealPathFromURI(imageUri));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Publics.toast("gallery error!");
                }
            }

        }
    }


    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }




/*
    public static String getRealPathFromURI_API19(Context context, Uri uri){
        String filePath = "";
        String wholeID = DocumentsContract.getDocumentId(uri);

        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];

        String[] column = { MediaStore.Images.Media.DATA };

        // where id is equal to
        String sel = MediaStore.Images.Media._ID + "=?";

        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                column, sel, new String[]{ id }, null);

        int columnIndex = cursor.getColumnIndex(column[0]);

        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }

    // search:
    public static String getRealPathFromURI_API11to18(Context context, Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        String result = null;

        CursorLoader cursorLoader = new CursorLoader(
                context,
                contentUri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        if(cursor != null){
            int column_index =
                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
        }
        return result;
    }
*/


    void openCustomDialog() {
        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.custom_dialog_camera_gallery);

        dialog.findViewById(R.id.linearCamera).setOnClickListener((View v) -> {
            if (checkCameraPermission())
                dialog.dismiss();
        });
        dialog.findViewById(R.id.linearGallery).setOnClickListener((View v) -> {
            if (checkGalleryPermission())
                dialog.dismiss();
        });
        dialog.show();
    }




    void addUserToDB(RegistrationInfoModel registrationInfoModel_1) {
          RegistrationInfoModel registrationInfoModel = new RegistrationInfoModel();
        registrationInfoModel.setLastname(registrationInfoModel_1.getLastname());
        registrationInfoModel.setAvatarpath(registrationInfoModel_1.getAvatarpath());
        registrationInfoModel.setName(registrationInfoModel_1.getName());
        registrationInfoModel.setEmail(registrationInfoModel_1.getEmail());
        registrationInfoModel.save();


    }
    void gotoRecyclerActivity() {

      Intent intent = new Intent(mContext, RecyclerActivity.class);
      startActivity(intent);
    }



}
