package basic.android.registerform.recycler;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;

import java.util.ArrayList;
import java.util.List;

import basic.android.registerform.R;
import basic.android.registerform.customViews.MyEditText;
import basic.android.registerform.utils.BaseActivity;
import basic.android.registerform.utils.Publics;

public class RecyclerActivity extends BaseActivity {

    RecyclerView recyclerView;
    List<RegistrationInfoModel> registrationInfoModelList;
    MyEditText word;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        bind();
        setAdapter();
    }

    void bind() {
        recyclerView = findViewById(R.id.recyclerView);
        word = findViewById(R.id.word);
        //   Publics.closeKeyBoard(word);
        word.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence input, int i, int i1, int i2) {
                filterRecycler(input.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }


    void filterRecycler(String input) {
        List<RegistrationInfoModel> tempRegistrationInfoModelList = new ArrayList<>();

        for (RegistrationInfoModel rm : registrationInfoModelList)
            if (rm.getName().toLowerCase().contains(input.toLowerCase()))
                tempRegistrationInfoModelList.add(rm);


        if (tempRegistrationInfoModelList.size() > 0) {
            RecyclerAdapter adapter = new RecyclerAdapter(tempRegistrationInfoModelList, mContext);
            recyclerView.setAdapter(adapter);
        }

    }


    void setAdapter() {
        registrationInfoModelList = new ArrayList<>();
        registrationInfoModelList = loadAllUsersFromDb();


        RecyclerAdapter adapter = new RecyclerAdapter(registrationInfoModelList, mContext);
        recyclerView.setAdapter(adapter);


    }

    List<RegistrationInfoModel> loadAllUsersFromDb() {


        for (int i = 0; i < dbCount(); i++) {
            RegistrationInfoModel registrationInfoModel = RegistrationInfoModel.findById(RegistrationInfoModel.class, i + 1);

        }


        List<RegistrationInfoModel> list = RegistrationInfoModel.listAll(RegistrationInfoModel.class);

        return list;
    }

    long dbCount() {
        return RegistrationInfoModel.count(RegistrationInfoModel.class, null, null);
    }

}
