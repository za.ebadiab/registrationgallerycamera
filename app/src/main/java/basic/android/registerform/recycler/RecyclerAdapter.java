package basic.android.registerform.recycler;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.io.File;
import java.util.List;

import basic.android.registerform.R;
import basic.android.registerform.customViews.MyTextView;
import basic.android.registerform.utils.Publics;
import de.hdodenhof.circleimageview.CircleImageView;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.Holder> {

    private List<RegistrationInfoModel> registrationInfoModels;
    private Context mContext;


    public RecyclerAdapter(List<RegistrationInfoModel> registrationInfoModels, Context mContext) {
        this.registrationInfoModels = registrationInfoModels;
        this.mContext = mContext;
    }

    @Override
    public RecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(mContext).inflate(R.layout.registration_info_recycler_item, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapter.Holder holder, int position) {

        holder.txtName.setText(registrationInfoModels.get(position).getName());

        holder.txtLastname.setText(registrationInfoModels.get(position).getLastname());
        holder.txtEmail.setText(registrationInfoModels.get(position).getEmail());

        if (registrationInfoModels.get(position) != null) {
            Toast.makeText(mContext, registrationInfoModels.get(position).getAvatarpath(), Toast.LENGTH_SHORT).show();

        }


        if (loadImageFromPath(registrationInfoModels.get(position).getAvatarpath()) != null)
            holder.imgAvatar.setImageBitmap(loadImageFromPath(registrationInfoModels.get(position).getAvatarpath()));

    }

    @Override
    public int getItemCount() {
        return registrationInfoModels.size();
    }


    Bitmap loadImageFromPath(String path) {


        File imgFile = new File(path);

        if (imgFile.exists()) {

            Bitmap bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

            return bitmap;
        }
        return null;
    }

    class Holder extends RecyclerView.ViewHolder {

        CircleImageView imgAvatar;
        MyTextView txtName, txtLastname, txtEmail;


        public Holder(View itemView) {
            super(itemView);

            imgAvatar = (CircleImageView) itemView.findViewById(R.id.imgAvatar);
            txtName = (MyTextView) itemView.findViewById(R.id.txtName);

            txtLastname = (MyTextView) itemView.findViewById(R.id.txtLastname);
            txtEmail = (MyTextView) itemView.findViewById(R.id.txtEmail);


            imgAvatar.setOnClickListener(v -> {

                RegistrationInfoModel registrationInfoModel = registrationInfoModels.get(getAdapterPosition());

                Publics.toast("clicked on: " + registrationInfoModel.getName());
            });


        }
    }
}
