package basic.android.registerform.recycler;

import com.orm.SugarRecord;


public class RegistrationInfoModel extends SugarRecord {


    private String name;
    private String lastname;
    private String email;
    private String avatarpath;
    private String username;
    private String password;


    public RegistrationInfoModel() {
    }

    public RegistrationInfoModel(String name, String lastname, String email, String avatarpath, String username, String password, byte[] image) {
        this.name = name;
        this.lastname = lastname;
        this.email = email;
        this.avatarpath = avatarpath;
        this.username = username;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    @Override
    public String toString() {
        return "RegistrationInfoModel{" +
                "name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public String getAvatarpath() {
        return avatarpath;
    }

    public void setAvatarpath(String avatarpath) {
        this.avatarpath = avatarpath;
    }


}
